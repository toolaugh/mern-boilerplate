/**
 * Created by NguyenBa on 4/4/2018.
 */
var User = require('../models/User');
exports.signUp = (req, res, next) => {
    /* Step to register new account
     * check account is exist
     * save account
     *
     * */
    const {
        firstName,
        lastName,
        email,
        password,
    } = req.body;
    if (!firstName) {
        return res.send({
            success: false,
            message: 'Error, first name is not null',
        });
    }
    if (!lastName) {
        return res.send({
            success: false,
            message: 'Error, last name is not null',
        });
    }
    if (!password) {
        return res.send({
            success: false,
            message: 'Error, password is not null',
        });
    }
    if (!email) {
        return res.send({
            success: false,
            message: 'Error, email is not null',
        });
    }
    /* Varify user is exist or not */
    User.find({ email: email.toLowerCase() }, (error, user) => {
        if (error) {
            return res.send({
                success: false,
                message: 'Error, user is exist',
            });
        }
        if (user.length > 0) {
            return res.send({
                success: false,
                message: 'Error, user is exist',
            });
        }
        var newUser = new User();
        newUser.firstName = firstName;
        newUser.lastName = lastName;
        newUser.email = email;
        newUser.password = password;
        newUser.save((error, user) => {
            if (error) {
                return res.send({
                    success: false,
                    message: 'Error, Some thing went wrong',
                });
            }
            return res.send({
                success: true,
                message: 'Sign up success',
                user,
            });
        });
    });
};
exports.logIn = (req, res, next) => {
    const {
        email,
        password,
    } = req.body;
    /* step to verify login */
    if (!email) {
        return res.send({
            success: false,
            message: 'Error, email is invalid',
        });
    }
    if (!password) {
        return res.send({
            success: false,
            message: 'Error, Password is invalid',
        });
    }
    User.find({ email: email.toLowerCase() }, (error, user) => {
        if (error) {
            return res.send({
                success: false,
                message: 'Error, server is error',
            });
        }
        if (user.length) {
            return res.send({
                success: true,
                user,
            });
        } else {
            return res.send({
                success: false,
                message: 'Error, Account is not exist',
            });
        }
    });
};

