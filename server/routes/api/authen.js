/**
 * Created by NguyenBa on 4/4/2018.
 */
var User = require('../../models/User');
var Authen = require('../../controller/Authen');

module.exports = (app) => {
    /*app.post('/api/account/sign_up', (req, res, next) => {
     const { body } = req;
     const {
     firstName,
     lastName,
     email,
     password,
     } = body;
     if (!firstName) {
     return res.send({
     success: false,
     message: 'Error, First name can not be null',
     });
     }
     if (!lastName) {
     return res.send({
     success: false,
     message: 'Error, Last name is not null',
     });
     }
     if (!email) {
     return res.send({
     success: false,
     message: 'Error, Email is not null',
     });
     }
     if (!password) {
     return res.send({
     success: fasle,
     message: 'Error, password null',
     });
     }
     /!* Valify user is exist or not *!/
     User.find({ email: email.toLowerCase() }, (error, prevUser) => {
     if (error) {
     return res.send({
     success: false,
     message: 'Error, Server is error',
     });
     } else if (prevUser.length > 0) {
     return res.send({
     success: false,
     message: 'Error, Account has been existed',
     });
     } else {
     /!* Save new User *!/
     const newUser = new User();
     newUser.firstName = firstName;
     newUser.lastName = lastName;
     newUser.email = email;
     newUser.password = newUser.generateHash(password);
     newUser.save((error, user) => {
     if (error) {
     return res.send({
     success: false,
     message: 'Error, Server error',
     });
     } else {
     return res.send({
     success: true,
     message: 'Signed up success',
     user,
     });
     }
     });
     }
     });
     });*/
    app.post('/api/account/sign_up', Authen.signUp);
    app.post('/api/account/login', Authen.logIn);
};