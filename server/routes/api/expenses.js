var Expense = require('../../models/Expense');
module.exports = (app) => {
    app.put('/api/insert', function (req, res) {
        var expense = new Expense();
        expense.description = req.body.desc;
        expense.amount = req.body.amount;
        expense.month = req.body.month;
        expense.year = req.body.year;
        expense.save(function (err) {
            if (err) {
                res.send(err);
            } else {
                res.send('Expense successfully added!');
            }
        });
    });
    app.put('/api/update', function (req, res) {
        const doc = {
            description: req.body.description,
            amount: req.body.amount,
            month: req.body.month,
            year: req.body.year,
        };
        console.log(doc);
        Expense.update({ _id: req.body._id }, doc, function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.send('Expense successfully updated!');
            }
        });
    });

    app.get('/api/delete', function (req, res) {
        var id = req.query.id;
        Expense.find({ _id: id }).remove().exec(function (err, expense) {
            if (err) {
                res.send(err);
            } else {
                res.send('Expense successfully deleted!');
            }
        });
    });
    app.get('/api/getAll', function (req, res) {
        var monthRec = req.query.month;
        var yearRec = req.query.year;
        if (monthRec && monthRec != 'All') {
            Expense.find({ $and: [{ month: monthRec }, { year: yearRec }] }, function (err, expenses) {
                if (err) {
                    res.send(err);
                } else {
                    res.json(expenses);
                }
            });
        } else {
            Expense.find({ year: yearRec }, function (err, expenses) {
                if (err)
                    res.send(err);
                res.json(expenses);
            });
        }
    });
};
