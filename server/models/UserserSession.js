/**
 * Created by NguyenBa on 4/4/2018.
 */
const mongoose = require('mongoose');
const UserSessionSchema = new mongoose.Schema({
    userId: {
        type: Number,
        default: -1,
    },
    timestamp: {
        type: Date,
        default: Date.now(),
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
});
module.exports = mongoose.model('UserSession', UserSessionSchema);