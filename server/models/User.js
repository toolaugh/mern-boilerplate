/**
 * Created by NguyenBa on 4/4/2018.
 */
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const UserSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    isDeleted: Boolean,
});
UserSchema.methods.generateHash = function (password) {
    return bcrypt.compareSync(password, bcrypt.genSaltSync(15), null);
};
UserSchema.methods.validatePassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};
module.exports = mongoose.model('User', UserSchema);