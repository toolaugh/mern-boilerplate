const nodemon = require('nodemon');
const path = require('path');
nodemon({
    execMap: {
        js: 'node',
    },
    script: path.join(__dirname, 'server/server'),
    ignore: [],
    watch: process.env.NODE_ENV !== 'production' ? ['server/*'] : false,
    ext: 'js',
})
    .on('start', function () {
        console.log('Server started!');
    })
    .on('crash', function (error) {
        console.log('Script crashed for some reasons', error);
    })
    .once('exit', function () {
        console.log('Shutting down server');
        process.exit();
    });
/* force a restart */
nodemon.emit('restart');

