const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const helpers = require('./helpers');

const NODE_ENV = process.env.NODE_ENV;
const isProd = NODE_ENV === 'production';
module.exports = {
    entry: {
        app: [
            helpers.root('client/app/index.js'),
        ],
    },
    output: {
        path: helpers.root('dist'),
        publicPath: '/',
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.css', '.scss', '.html'],
        alias: {
            'app': 'client/app',
        },
    },
    module: {
        rules: [
            // JSX files
            {
                test: /\.jsx?$/,
                include: helpers.root('client'),
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react'],
                },
            },

            // SCSS files
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                'sourceMap': true,
                                'importLoaders': 1,
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: () => [
                                    autoprefixer,
                                ],
                            },
                        },
                        'sass-loader',
                    ],
                }),
            },
            /* {
             test: /\.(scss)$/,
             use: [{
             loader: 'style-loader', // inject CSS to page
             }, {
             loader: 'css-loader', // translates CSS into CommonJS modules
             }, {
             loader: 'postcss-loader', // Run post css actions
             options: {
             plugins: function () { // post css plugins, can be exported to postcss.config.js
             return [
             require('precss'),
             require('autoprefixer'),
             ];
             },
             },
             }, {
             loader: 'sass-loader' // compiles Sass to CSS
             }],
             },*/
            //CSS
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            /* config for semantic ui -react css
             * {test: /\.(png|woff|woff2|eot|ttf|svg)$/,loader: 'url-loader?limit=100000'}
             * And you need to npm i url-loader file-loader -S*/
            {
             test: /\.(png|woff|woff2|eot|ttf|svg)$/,
             loader: 'url-loader?limit=100000',
             },
        ],
    },
    plugins: [
        // new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),

        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(NODE_ENV),
            },
        }),

        new HtmlWebpackPlugin({
            template: helpers.root('client/public/index.html'),
            inject: 'body',
        }),

        new ExtractTextPlugin({
            filename: 'css/[name].[hash].css',
            disable: !isProd,
        }),

        new CopyWebpackPlugin([{
            from: helpers.root('client/public'),
        }]),
    ],
};
