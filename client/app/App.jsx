import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import HomePage from './components/_home_pages/HomePage';
import LoginPage from './components/_auth_pages/LoginPage';
import RegisterPage from './components/_auth_pages/RegisterPage';
import ErrorPage from './components/ErrorPage';
const App = () => {
    return (
        <Switch>
            <Route path="/" exact component={HomePage}/>
            <Route path="/register" component={RegisterPage}/>
            <Route path="/login" component={LoginPage}/>
            <Route
                render={() => <ErrorPage
                    error={{
                        code: 4,
                        error: 'Error page',
                    }}
                />}
            />
        </Switch>
    );
};

export default App;
